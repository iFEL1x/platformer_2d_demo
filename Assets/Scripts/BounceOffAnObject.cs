﻿using System.Collections;
using System;
using UnityEngine;

/* Класс объекта "BoostJump" группы "EnemyArea\Enemy" и "PlatformJump" придающий усиленный отскок */

public class BounceOffAnObject : MonoBehaviour
{
    [Tooltip("Сила отскока от объекта.")]
    [SerializeField] private int force;
    [Tooltip("Время кулдауна на повторный отскок от объекта.")]
    [SerializeField] private float isCouldown = 2f;
    [SerializeField] private Animator animator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player _player = collision.gameObject.GetComponent<Player>();

            if(animator != null)
            {
                animator.SetTrigger("isGrounded");
            }
            else //Отключаем на "Enemy" спрайт отображащий разрешение на отскок.
            {
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
            StartCoroutine(PauseBoost(_player));
        }
    }

    //Карутина кулдауна на повторный отскок.
    private IEnumerator PauseBoost(Player _player)
    {
        _player.Jump(force);
        gameObject.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(isCouldown);
        gameObject.GetComponent<Collider2D>().enabled = true;
        
        if(animator == null)
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true; //Включаем на "Enemy" спрайт отображащий разрешение на отскок.
        }
        yield break;
    }
}
